﻿namespace PrairieKingPrizes.Framework
{
    internal class PrizeTier
    {
        public double Chance { get; set; }
        public Prize[] Prizes { get; set; }
    }
}
